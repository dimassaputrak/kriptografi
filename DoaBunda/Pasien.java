public class Pasien
{
    private String name;
    private int age;
    private String jenisOperasi;

    public Pasien(String n, int a, String jo){
        this.name = n;
        this.age = a;
        this.jenisOperasi = jo;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String n){
        this.name = n;
    }

    public int getAge(){
        return this.age;
    }

    public void setAge(int aa){
        this.age = aa;
    }

    public String getJenisOperasi(){
        return this.jenisOperasi;
    }

    public void setJenisOperasi(String jo){
        this.jenisOperasi = jo;
    }
}
