import java.util.*;

public class WeekDays
{   
    private String namaHari;
    private ArrayList<Pasien> todaysPasien;
    private ArrayList<Doctor> docsOfTheDay;
    private ArrayList<String> pasangan;
    private boolean available;

    public WeekDays(String nama){
        this.namaHari = nama;
        this.todaysPasien = new ArrayList<Pasien>();
        this.docsOfTheDay = new ArrayList<Doctor>();
        this.pasangan = new ArrayList<String>();
        this.available = true;
    }

    public String getNamaHari(){
        return this.namaHari;
    }

    public void setNamaHari(String n){
        this.namaHari = n;
    }

    public ArrayList<Pasien> getPasien(){
        return this.todaysPasien;
    }

    public ArrayList<Doctor> getDokter(){
        return this.docsOfTheDay;
    }

    public ArrayList<String> getPasangan(){
        return this.pasangan;
    }

    public boolean getAvailability(){
        return this.available;
    }

    public void setAvailability(boolean a){
        this.available = a;
    }
}
