public class Doctor
{
    private String docname;
    private String spesialisasi;

    public Doctor(String name, String spe){
        this.docname = name;
        this.spesialisasi = spe;
    }

    public String getDocName(){
        return this.docname;
    }

    public void setDocName(String name){
        this.docname = name;
    }

    public String getSpesialisasi(){
        return this.spesialisasi;
    }

    public void setSpesialisasi(String spe){
        this.spesialisasi = spe;
    }
}
