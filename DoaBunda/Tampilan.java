import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;

public class Tampilan extends JFrame
{
    JLabel namaPasien;
    JLabel umurPasien;
    JLabel jsOpe;
    JLabel dayOper;

    JComboBox<String> jenisOperasi;
    JComboBox<String> dayProposed;

    JTextField nama;
    JTextField umur;

    JPanel panelnya = new JPanel();

    JButton button;

    ActionListener listener;

    // attribute
    String n;
    int a;
    String js;
    String day;

    Schedule s;
    Pasien p;
    public Tampilan(Schedule s){
        class AddListener implements ActionListener
        {
            public void actionPerformed(ActionEvent action){
                n = nama.getText();
                a = Integer.parseInt(umur.getText());

                js = (String)jenisOperasi.getSelectedItem();
                day = (String)dayProposed.getSelectedItem();

                //s = new Schedule();

                p = new Pasien(n, a, js);

                s.getSolution(p, day);

                String temp = "";
                for(int i = 0; i<s.getSeminggu().size(); i++){
                    for(int j=0; j<s.getSeminggu().get(i).getPasien().size(); j++){
                        if(s.getSeminggu().get(i).getPasien().get(j).getName().equalsIgnoreCase(n)){
                            temp = "Hasil: "+n+", "+s.getSeminggu().get(i).getNamaHari()+
                                " slot no "+(j+1)+", dr. "+s.getSeminggu().get(i).getDokter().get(j).getDocName();
                        }
                    }
                    System.out.println(s.getSeminggu().get(i).getNamaHari() + s.getSeminggu().get(i).getPasangan());
                }
                JOptionPane.showMessageDialog(null, temp, "Hasil", JOptionPane.INFORMATION_MESSAGE);
                setVisible(true);
            }
        }
        listener = new AddListener();
        createTextField();
        createButton();
        createPanel();

    }

    public void createTextField(){
        namaPasien = new JLabel("Nama pasien");
        nama = new JTextField();

        umurPasien = new JLabel("Umur pasien");
        umur = new JTextField();

        jsOpe = new JLabel("Jenis operasi");
        String[] o = {"Jantung", "Tulang", "Kulit", "Gigi"};
        jenisOperasi = new JComboBox<String>(o);

        dayOper = new JLabel("Hari");
        String[] days = {"Senin", "Selasa", "Rabu", "Kamis", "Jumat"};
        dayProposed = new JComboBox<String>(days);
    }

    public void createButton(){
        button = new JButton("Schedule it now!");
        button.addActionListener(listener);
    }

    public void createPanel(){
        panelnya.setLayout(new GridLayout(5,2));
        panelnya.add(namaPasien);
        panelnya.add(nama);
        panelnya.add(umurPasien);
        panelnya.add(umur);
        panelnya.add(jsOpe);
        panelnya.add(jenisOperasi);
        panelnya.add(dayOper);
        panelnya.add(dayProposed);
        panelnya.add(button);
        panelnya.setBorder(new TitledBorder(new EtchedBorder(), "Scheduling"));

        add(panelnya, BorderLayout.CENTER);
    }
}