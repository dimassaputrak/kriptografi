import java.util.*;

class Schedule
{
    final int OPERASI = 3;
    final int WORKDAYS = 5;
    ArrayList<WeekDays> seminggu;
    ArrayList<Doctor> docs;

    public Schedule(){
        this.seminggu = new ArrayList<WeekDays>();
        this.setAWeek();
        this.docs = new ArrayList<Doctor>();
        this.setDoctors();
    }

    public void setAWeek(){
        this.seminggu.add(new WeekDays("Senin"));
        this.seminggu.add(new WeekDays("Selasa"));
        this.seminggu.add(new WeekDays("Rabu"));
        this.seminggu.add(new WeekDays("Kamis"));
        this.seminggu.add(new WeekDays("Jumat"));
    }

    public ArrayList<WeekDays> getSeminggu(){
        return this.seminggu;
    }

    public void setDoctors(){
        this.docs.add(new Doctor("Aldo", "Jantung"));
        this.docs.add(new Doctor("Budi", "Tulang"));
        this.docs.add(new Doctor("Caca", "Kulit"));
        this.docs.add(new Doctor("Diah", "Gigi"));
    }

    public ArrayList<Doctor> getAllDoctors(){
        return this.docs;
    }

    public void getSolution(Pasien newPasien, String hari){
        String tmp = "";
        for(int i = 0; i<seminggu.size(); i++){
            if(hari.equalsIgnoreCase(seminggu.get(i).getNamaHari())){
                String name = newPasien.getName();
                int age = newPasien.getAge();
                String jsOperasi = newPasien.getJenisOperasi();

                if(seminggu.get(i).getPasangan().size() == OPERASI){
                    if(seminggu.get(i).getPasangan().get(1).equals("")){ //slot ke 2 kosong

                        // check dokternya sama atau tidak
                        getSolutionFreeSlot(newPasien, 1, i);
                    } else if(seminggu.get(i).getPasangan().get(2).equals("")){
                        getSolutionFreeSlot(newPasien, 2, i);
                    }
                    else {
                        // next day
                        getSolutionNextDay(newPasien, i+1);
                    }
                } else if(seminggu.get(i).getPasangan().size() < OPERASI){
                    int sz = seminggu.get(i).getPasangan().size();
                    if(sz == 0){
                        // just add
                        for(int j = 0; j<docs.size(); j++){
                            String spl = docs.get(j).getSpesialisasi();
                            if(jsOperasi.equalsIgnoreCase(spl)){
                                tmp = name + "-" + age + "-" + jsOperasi + "-" + docs.get(j).getDocName();
                                seminggu.get(i).getPasien().add(newPasien);
                                seminggu.get(i).getDokter().add(docs.get(j));
                                seminggu.get(i).getPasangan().add(tmp);
                            }
                        }
                    } else {
                        if(jsOperasi.equalsIgnoreCase(seminggu.get(i).getDokter().get(sz-1).getSpesialisasi())){
                            // adding dummy on this slot
                            seminggu.get(i).getPasien().add(new Pasien("", 0, ""));
                            seminggu.get(i).getDokter().add(new Doctor("", ""));
                            seminggu.get(i).getPasangan().add(tmp);

                            // then add itself to the next slot
                            sz = seminggu.get(i).getPasangan().size();
                            if(sz < OPERASI){
                                for(int j = 0; j<docs.size(); j++){
                                    String spl = docs.get(j).getSpesialisasi();
                                    if(jsOperasi.equalsIgnoreCase(spl)){
                                        tmp = name + "-" + age + "-" + jsOperasi + "-" + docs.get(j).getDocName();
                                        seminggu.get(i).getPasien().add(newPasien);
                                        seminggu.get(i).getDokter().add(docs.get(j));
                                        seminggu.get(i).getPasangan().add(tmp);
                                    }
                                }
                            } else {
                                // next day
                                getSolutionNextDay(newPasien, i+1);
                            }
                        } else {
                            for(int j = 0; j<docs.size(); j++){
                                String spl = docs.get(j).getSpesialisasi();
                                if(jsOperasi.equalsIgnoreCase(spl)){
                                    tmp = name + "-" + age + "-" + jsOperasi + "-" + docs.get(j).getDocName();
                                    seminggu.get(i).getPasien().add(newPasien);
                                    seminggu.get(i).getDokter().add(docs.get(j));
                                    seminggu.get(i).getPasangan().add(tmp);
                                }
                            }
                        }
                    }
                } else {
                    getSolutionNextDay(newPasien, i+1);
                }
            }
        }
    }

    public void getSolutionNextDay(Pasien newPasien, int indexDay){
        String tmp = "";
        for(int i = 0; i<seminggu.size(); i++){
            if(i==indexDay){
                String name = newPasien.getName();
                int age = newPasien.getAge();
                String jsOperasi = newPasien.getJenisOperasi();

                // check slot isinya 3, tapi ada yg kosong?
                if(seminggu.get(i).getPasangan().size() == OPERASI){

                    // check yg kosong
                    if(seminggu.get(i).getPasangan().get(1).equals("")){ //slot ke 2 kosong
                        // check dokternya sama atau tidak
                        getSolutionFreeSlot(newPasien, 1, i);
                    } else if(seminggu.get(i).getPasangan().get(2).equals("")){
                        getSolutionFreeSlot(newPasien, 2, i);
                    } else {
                        getSolutionNextDay(newPasien, i+1);
                    }
                } else if(seminggu.get(i).getPasangan().size() < OPERASI){
                    // check masing kosong atau engga
                    int sz = seminggu.get(i).getPasangan().size();
                    if(sz == 0){
                        // just add
                        for(int j = 0; j<docs.size(); j++){
                            String spl = docs.get(j).getSpesialisasi();
                            if(jsOperasi.equalsIgnoreCase(spl)){
                                tmp = name + "-" + age + "-" + jsOperasi + "-" + docs.get(j).getDocName();
                                seminggu.get(i).getPasien().add(newPasien);
                                seminggu.get(i).getDokter().add(docs.get(j));
                                seminggu.get(i).getPasangan().add(tmp);
                            }
                        }
                    } else {
                        if(jsOperasi.equalsIgnoreCase(seminggu.get(i).getDokter().get(sz-1).getSpesialisasi())){
                            // adding dummy on this slot
                            seminggu.get(i).getPasien().add(new Pasien("", 0, ""));
                            seminggu.get(i).getDokter().add(new Doctor("", ""));
                            seminggu.get(i).getPasangan().add(tmp);

                            // then add itself to the next slot
                            sz = seminggu.get(i).getPasangan().size();
                            if(sz < OPERASI){
                                for(int j = 0; j<docs.size(); j++){
                                    String spl = docs.get(j).getSpesialisasi();
                                    if(jsOperasi.equalsIgnoreCase(spl)){
                                        tmp = name + "-" + age + "-" + jsOperasi + "-" + docs.get(j).getDocName();
                                        seminggu.get(i).getPasien().add(newPasien);
                                        seminggu.get(i).getDokter().add(docs.get(j));
                                        seminggu.get(i).getPasangan().add(tmp);
                                    }
                                }
                            } else {
                                // next day
                                getSolutionNextDay(newPasien, i+1);
                            }
                        } else {
                            for(int j = 0; j<docs.size(); j++){
                                String spl = docs.get(j).getSpesialisasi();
                                if(jsOperasi.equalsIgnoreCase(spl)){
                                    tmp = name + "-" + age + "-" + jsOperasi + "-" + docs.get(j).getDocName();
                                    seminggu.get(i).getPasien().add(newPasien);
                                    seminggu.get(i).getDokter().add(docs.get(j));
                                    seminggu.get(i).getPasangan().add(tmp);
                                }
                            }
                        }
                    }
                } else {
                    getSolutionNextDay(newPasien, i+1);
                }
            }
        }
    }

    public void getSolutionFreeSlot(Pasien p, int index, int iday){
        String name = p.getName();
        int age = p.getAge();
        String jsOperasi = p.getJenisOperasi();

        String tmp = "";
        if(jsOperasi.equalsIgnoreCase(seminggu.get(iday).getDokter().get(index-1).getSpesialisasi())){
            // next day
            getSolutionNextDay(p, iday+1);
        } else {
            // change data everything of the slot
            for(int j = 0; j<docs.size(); j++){
                String spl = docs.get(j).getSpesialisasi();
                if(jsOperasi.equalsIgnoreCase(spl)){
                    tmp = name + "-" + age + "-" + jsOperasi + "-" + docs.get(j).getDocName();

                    seminggu.get(iday).getPasien().get(index).setName(p.getName());
                    seminggu.get(iday).getPasien().get(index).setAge(p.getAge());
                    seminggu.get(iday).getPasien().get(index).setJenisOperasi(p.getJenisOperasi());

                    seminggu.get(iday).getDokter().get(index).setDocName(docs.get(j).getDocName());
                    seminggu.get(iday).getDokter().get(index).setSpesialisasi(docs.get(j).getSpesialisasi());

                    seminggu.get(iday).getPasangan().set(index, tmp);
                }
            }
        }
    }
}