import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;
import java.io.*;

/*
    This program implements GUI for XTS-AES Calculator
*/
public class Tampilan extends JPanel
{
    JPanel panelnya = new JPanel();
    JPanel encryptionPanel = new JPanel();
    JPanel decryptionPanel = new JPanel();

    Label mainTitle, encryptionTitle, decryptionTitle;

    JLabel welcome, question, plaintextInput, pathPlaintextInput, keyEncrypt, pathKeyEncrypt, ciphertextInput, pathCiphertextInput, keyDecrypt, pathKeyDecrypt;
    JButton encryptionButton, decryptionButton, plaintextButton, keyEncryptButton, ciphertextButton, keyDecryptButton, encryptButton, decryptButton, backEnButton, backDeButton;

    /*
        Constructor
    */
    public Tampilan(){
        super(new BorderLayout());
        createTitle();
        createLabel();
        createButton();
        createPanel();
    }

    /*
        Creating title
    */
    public void createTitle(){
        // main menu title
        mainTitle = new Label("XTS-AES Calculator");
        mainTitle.setAlignment(Label.CENTER);
        mainTitle.setFont(new Font("Amaranth", Font.PLAIN, 20));
        mainTitle.setBounds(30, 10, 414, 25);

        // encryption title
        encryptionTitle = new Label("Encryption");
        encryptionTitle.setAlignment(Label.CENTER);
        encryptionTitle.setFont(new Font("Amaranth", Font.PLAIN, 20));
        encryptionTitle.setBounds(30, 10, 414, 25);

        // decryption title
        decryptionTitle = new Label("Decryption");
        decryptionTitle.setAlignment(Label.CENTER);
        decryptionTitle.setFont(new Font("Amaranth", Font.PLAIN, 20));
        decryptionTitle.setBounds(30, 10, 414, 25);
    }

    /*
        Creating label for each
    */
    public void createLabel(){
        // main menu label
        welcome = new JLabel("Welcome!");
        welcome.setBounds(210, 62, 100, 20);

        question = new JLabel("What do you want to do?");
        question.setBounds(170, 85, 160, 20);

        // encryption label
        plaintextInput = new JLabel("Plaintext File");
        plaintextInput.setBounds(50, 62, 100, 20);

        keyEncrypt = new JLabel("Key File (in Hex)");
        keyEncrypt.setBounds(50, 100, 100, 20);

        pathPlaintextInput = new JLabel("(no file selected)");
        pathPlaintextInput.setBounds(180, 62, 110, 20);
        
        pathKeyEncrypt = new JLabel("(no file selected)");
        pathKeyEncrypt.setBounds(180, 100, 110, 20);

        // decryption label
        ciphertextInput = new JLabel("Ciphertext File");
        ciphertextInput.setBounds(50, 62, 100, 20);

        keyDecrypt = new JLabel("Key File (in Hex)");
        keyDecrypt.setBounds(50, 100, 100, 20);

        pathCiphertextInput = new JLabel("(no file selected)");
        pathCiphertextInput.setBounds(180, 62, 110, 20);
        
        pathKeyDecrypt = new JLabel("(no file selected)");
        pathKeyDecrypt.setBounds(180, 100, 110, 20);
    }

    /*
        Creating every button including action listener
    */
    public void createButton(){
        // main menu button
        encryptionButton = new JButton("Encrypt");
        encryptionButton.setBounds(130, 131, 209, 35);

        decryptionButton = new JButton("Decrypt");
        decryptionButton.setBounds(130, 171, 209, 35);

        // encryption button
        plaintextButton = new JButton("Browse");
        plaintextButton.setBounds(347, 61, 83, 23);

        keyEncryptButton = new JButton("Browse");
        keyEncryptButton.setBounds(347, 99, 83, 23);

        encryptButton = new JButton("Encrypt and Save");
        encryptButton.setBounds(135, 155, 209, 35);

        backEnButton = new JButton("Back");
        backEnButton.setBounds(135, 200, 209, 35);

        // decryption button
        ciphertextButton = new JButton("Browse");
        ciphertextButton.setBounds(347, 61, 83, 23);

        keyDecryptButton = new JButton("Browse");
        keyDecryptButton.setBounds(347, 99, 83, 23);

        decryptButton = new JButton("Decrypt and Save");
        decryptButton.setBounds(135, 155, 209, 35);

        backDeButton = new JButton("Back");
        backDeButton.setBounds(135, 200, 209, 35);

        plaintextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                chooseFile(pathPlaintextInput);
            }
        });

        keyEncryptButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                chooseFile(pathKeyEncrypt);
            }
        });

        ciphertextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                chooseFile(pathCiphertextInput);
            }
        });

        keyDecryptButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                chooseFile(pathKeyDecrypt);
            }
        });

        encryptButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                try {
                    EnDecSave(pathPlaintextInput, pathKeyEncrypt, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        decryptButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                try {
                    EnDecSave(pathCiphertextInput, pathKeyDecrypt, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        decryptButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                try {
                    EnDecSave(pathCiphertextInput, pathKeyDecrypt, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        decryptButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                try {
                    EnDecSave(pathCiphertextInput, pathKeyDecrypt, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        decryptButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                try {
                    EnDecSave(pathCiphertextInput, pathKeyDecrypt, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        encryptionButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                remove(panelnya);
                createEncryptionPanel();

                repaint();
                revalidate();
            }
        });

        decryptionButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                remove(panelnya);
                createDecryptionPanel();

                repaint();
                revalidate();
            }
        });

        backEnButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                remove(encryptionPanel);
                add(panelnya);

                repaint();
                revalidate();
            }
        });

        backDeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                remove(decryptionPanel);
                add(panelnya);

                repaint();
                revalidate();
            }
        });
    }

    public void chooseFile(JLabel file){
        JFileChooser fileChooser = new JFileChooser();
        int result = fileChooser.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            file.setText(selectedFile.getAbsolutePath());
        }
    }

    /*
        A Function to save file on selected file chooser
    */
    public String saveMap() {
        JFileChooser chooser = new JFileChooser();
        int retrival = chooser.showSaveDialog(null);
        if (retrival == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile().getAbsolutePath();
        }
        return null;
    }

    /*
        Function to encrypt or decrypt the file and save it
    */
    public void EnDecSave(JLabel fileInput, JLabel keyInput, boolean dec) throws Exception{
        System.out.println("EnDecSave");
        try {
            if(fileInput.getText().equals("(no file selected)") || 
                keyInput.getText().equals("(no file selected)")){
                JOptionPane.showMessageDialog(null, "Files can not be empty", "Choose Files!", JOptionPane.INFORMATION_MESSAGE);    
            }
            else{
                FileReader fr = new FileReader(keyInput.getText());
                BufferedReader reader = new BufferedReader(fr);     
                String key = reader.readLine();
                
                if (key.length()==64 && key.matches("-?[0-9a-fA-F]+")){                    
                    String result = saveMap();
                    XTS xts = new XTS(fileInput.getText(), keyInput.getText(), result);    
                    if(dec){
                        xts.decrypt();  
                    } else {
                        xts.encrypt();      
                    }                    
                    JOptionPane.showMessageDialog(null, "File was successfully processed", "Success", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Key File must be filled with 64 digits Hex", "Wrong Input!", JOptionPane.INFORMATION_MESSAGE);
                }
                reader.close();
            }
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }        
    }

    /*
        Creating panel and adding components
    */
    public void createPanel(){
        panelnya.setBorder(new EmptyBorder(5, 5, 5, 5));
        panelnya.setBackground(new Color(202, 219,247));
        panelnya.setLayout(null);
        
        panelnya.add(mainTitle);

        panelnya.add(welcome);
        panelnya.add(question);

        panelnya.add(encryptionButton);
        panelnya.add(decryptionButton);

        add(panelnya);
    }

    public void createEncryptionPanel() {
        encryptionPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        encryptionPanel.setBackground(new Color(202, 219,247));
        encryptionPanel.setLayout(null);
        
        encryptionPanel.add(encryptionTitle);

        encryptionPanel.add(plaintextInput);
        encryptionPanel.add(pathPlaintextInput);
        encryptionPanel.add(plaintextButton);

        encryptionPanel.add(keyEncrypt);
        encryptionPanel.add(pathKeyEncrypt);
        encryptionPanel.add(keyEncryptButton);

        encryptionPanel.add(encryptButton);
        encryptionPanel.add(backEnButton);

        add(encryptionPanel);
    }

    public void createDecryptionPanel() {
        decryptionPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        decryptionPanel.setBackground(new Color(202, 219,247));
        decryptionPanel.setLayout(null);
        
        decryptionPanel.add(decryptionTitle);

        decryptionPanel.add(ciphertextInput);
        decryptionPanel.add(pathCiphertextInput);
        decryptionPanel.add(ciphertextButton);

        decryptionPanel.add(keyDecrypt);
        decryptionPanel.add(pathKeyDecrypt);
        decryptionPanel.add(keyDecryptButton);

        decryptionPanel.add(decryptButton);
        decryptionPanel.add(backDeButton);

        add(decryptionPanel);
    }
}