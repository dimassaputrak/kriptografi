XTS-AES Calculator

=================================================

Author:
- Dimas Saputra	            1506688821
- Fayya Nadhira Anyatasia	1506688790
- Qurrata A'yuna Adrianus	1506689010

This is a program that implements encryption and decryption of XTS-AES mode.

Manual:
1. Run program with commandLine to show user interface of the program
javax *.java
java MainProgram

2. Choose either Encryption or Decryption

3. If you choose Encryption tab then you can encrypt an input file by doing:
    - Click browse to get plaintext, then there will be printed the path of the file
    - Click browse to get key file, then there will be printed the path of the file
    - Click Encryption and Save to save your output file after encrypting. Don't forget the extension!

4. If you choose Decryption tab then you can decrypt an input file by doing:
    - Click browse to get ciphertext, then there will be printed the path of the file
    - Click browse to get key file, then there will be printed the path of the file
    - Click Decryption and Save to save your output file after decrypting. Don't forget the extension!

5. You are done!

Note: You can choose any type of file for plaintext (text, audio, image, etc) and your key must be 256 bits (64 digit of Hex).