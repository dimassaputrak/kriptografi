import java.util.*;
import java.text.*;
import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;

/*
    This is the main program which will show GUI
    @author Dimas Saputra
    @author Fayya Nadhira Anyatasia
    @author Qurrata A'yuna Adrianus
*/
public class MainProgram
{
    public static void main(String[] args) {
        // Create and set up the window.
        JFrame frame = new JFrame("XTS-AES");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 482, 300);
        frame.setTitle("XTS AES Calculator");
        
        // Create and set up the content pane.
        JComponent newContentPane = new Tampilan();
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);

        // Display the window.
        frame.setSize(500,300);
        frame.setVisible(true);   

        System.out.println("XTS AES encryption");
    }
}