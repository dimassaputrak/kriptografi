/*
	Represents utility to format data
*/
public class Utilization {
	public static final char[] HEX_DIGITS = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
	
	/*
		Convert hex to bytes
		@param str: a string
		@return array of byte
	*/
	public static byte[] hexToBytesConverter(String str) {
		if (str == null) {
			return null;
		} else if (str.length() < 2) {
			return null;
		} else {
			int len = str.length() / 2;
			byte[] buffer = new byte[len];
			for (int i = 0; i < len; i++) {
				buffer[i] = (byte) Integer.parseInt(
						str.substring(i * 2, i * 2 + 2), 16);
			}
			return buffer;
		}

	}

	/*
		Convert bytes to hex
		@param ba: array of bytes
		@return a new string
	*/	
	public static String bytesToHexConverter (byte[] ba) {
		int length = ba.length;
	    char[] buf = new char[length * 2];
	    for (int i = 0, j = 0, k; i < length; ) {
	        k = ba[i++];
	        buf[j++] = HEX_DIGITS[(k >>> 4) & 0x0F];
	        buf[j++] = HEX_DIGITS[ k        & 0x0F];
	    }
	    return new String(buf);
	}	
}
