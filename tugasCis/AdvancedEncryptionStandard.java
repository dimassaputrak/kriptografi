import java.security.*;
import javax.crypto.*;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/*
    This program implements AES using library crypto
*/
public class AdvancedEncryptionStandard {
    
    private String keyHex;

    /*
        Constructor
    */
    public AdvancedEncryptionStandard(byte[] keyHex) {
        this.keyHex = Utilization.bytesToHexConverter(keyHex);
    }

    /*
        A method to encrypt input file
        @param textHex: file is already converted to textHex
        @return result after encryption
    */    
    public byte[] encrypt(byte[] textHex)throws Exception {
        SecretKey key = new SecretKeySpec(DatatypeConverter.parseHexBinary(keyHex), "AES");
            
        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
            
        byte[] result = cipher.doFinal(DatatypeConverter.parseHexBinary(Utilization.bytesToHexConverter(textHex)));
            
        return result;
    }

    /*
        A method to decrypt input file
        @param textHex: file is already converted to textHex
        @return result after decryption
    */        
    public byte[] decrypt(byte[] textHex)throws Exception{
        
        SecretKey key = new SecretKeySpec(DatatypeConverter.parseHexBinary(keyHex), "AES");
            
        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, key);
            
        byte[] result = cipher.doFinal(DatatypeConverter.parseHexBinary(Utilization.bytesToHexConverter(textHex)));
            
        return result;
    }   
    
}